**What is a  Docker?**

An open-source project that automates the deployment of software applications inside containers by providing an additional layer of abstraction and automation of OS-level virtualization on Linux.

**Terminologies:-**

**1.Docker Image** - An image is a read-only template with instructions for creating a Docker container. It contains everything needed to run applications as a container. This includes:

- code
- runtime
- libraries
- environment variables
- configuration files

The image can then be deployed to any Docker environment and as a container.

**2.Container** - A container is a runnable instance of an image. We can create, start, stop, move, or delete a container using the Docker API or CLI.

**3.DockersHub** - Docker Hub is like GitHub but for docker images and containers.


**Block Diagram of a Docker Architecture:-**

![](https://static.packt-cdn.com/products/9781788992329/graphics/98f26def-8b4d-49f1-82ae-3341fa152e2e.png)

**Docker Basic Commands:-**

- $ docker ps :- allows us to view all the containers that are running on the Docker Host.
- $ docker start :- starts any stopped container(s).
- $ docker stop :-  stops any running container(s).
- $ docker run :- it creates containers from docker images.
- $ docker rm :-   it deletes the containers.
- $ docker cp :-  copy file inside docker.
- $ docker exec :-  give permission to run shell script.
- $ docker tag :- tag image name with different name.
- $ docker push :-  push on dockerhub.


**Common Operations on Dockers:-**

- Download/pull the docker images that you want to work with.
- Copy your code inside the docker
- Access docker terminal
- Install and additional required dependencies
- Compile and Run the Code inside docker
- Document steps to run your program in README.md file
- Commit the changes done to the docker.
- Push docker image to the docker-hub and share repository with people who want to try your code.

