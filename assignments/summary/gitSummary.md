**What is Git ?**

- Git is a version control (VCS) system for tracking changes to projects. 
- Version control systems are also called revision control systems or source code management (SCM) systems it is     used for tracking changes in source code during software development.
- Its goals include speed, data integrity, and support for distributed, nonlinear workflows.

**Git Workflow Block Diagram**

![](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)


**Getting started with Git**

- Instaling Git
For Linux: **sudo apt-get install git**

**Basic Git commands:**

- git clone :- clone the repository on local system.
- git add :- adds changes in the working directory to the staging area.
- git commit :- saves the files from staging area into the local Git repository.
- git push :- saves the files in local repo onto the remote repo.
- git pull :- fetch and merge changes on the remote server to your working directory.


